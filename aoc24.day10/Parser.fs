﻿module aoc24.day10.Parser

open Vass.Stuff
open Vass.Stuff.Parser
open Domain

let tile: Tile ParseFunc =
    character
    |> choose (function
        | c when '0' <= c && c <= '9' -> Some (int (c - '0'))
        | _ -> None)

let row = list tile |> filter (fun row -> List.length row > 0)

let inputParser =
    separatedList newline row
    |> map Grid.ofSeqs
