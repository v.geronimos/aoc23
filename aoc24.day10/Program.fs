﻿open Vass.Stuff
open aoc24.day10
open aoc24.day10.Domain

let solve = Solver.solve Parser.inputParser part1 part2

solve "example.txt"
solve "input.txt"