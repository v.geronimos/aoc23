﻿module aoc24.day10.Domain

open Vass.Stuff
open Vass.Stuff.Types

type Tile = int

let offsets = [-1, 0; 1, 0; 0, 1; 0, -1]
let getPaths (r, c) transform (map: Tile grid) =
    let rec getFrom currentNumber locations =
        if currentNumber = 9
        then locations
        else
            let nextNumber = currentNumber + 1

            locations
            |> List.collect (fun (r', c') ->
                offsets
                |> List.choose (fun (dr, dc) ->
                    let r'' = r' + dr
                    let c'' = c' + dc
                    match map.TryGetCell(r'', c'') with
                    | Some height when height = nextNumber -> Some (r'', c'')
                    | _ -> None
                    )
                )
            |> transform
            |> getFrom nextNumber

    getFrom 0 [(r, c)]

let part1 (input: Tile grid) =
    seq {
        for r in 0 .. input.Rows - 1 do
            for c in 0 .. input.Columns - 1 do
                if input[r, c] = 0
                then
                    let paths = getPaths (r, c) List.distinct input
                    yield List.length paths
    }
    |> Seq.sum
    |> Some


let part2 (input: Tile grid) =
    seq {
        for r in 0 .. input.Rows - 1 do
            for c in 0 .. input.Columns - 1 do
                if input[r, c] = 0
                then
                    let paths = getPaths (r, c) id input
                    yield List.length paths
    }
    |> Seq.sum
    |> Some
