﻿open Vass.Stuff
open TemplateProject
open TemplateProject.Domain

let solve = Solver.solve Parser.inputParser part1 part2

solve "example.txt"
solve "input.txt"
