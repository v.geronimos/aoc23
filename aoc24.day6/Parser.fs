﻿module aoc24.day6.Parser

open Vass.Stuff
open Vass.Stuff.Parser
open Domain

let tile = literalMap (Map.ofList [
    "#", (Obstruction, false)
    ".", (Passable, false)
    "^", (Passable, true)
])

let row =
    list tile
    |> map (fun tiles ->
        let guardIdx = tiles |> List.tryFindIndex snd
        (tiles |> List.map fst), guardIdx)

let inputParser =
    separatedList newline row
    |> map (fun rows ->
        let guardPos =
            rows
            |> Seq.mapi (fun r (_, col) -> col |> Option.map (fun c -> r, c))
            |> Seq.pick id

        let grid =
            rows
            |> List.map fst
            |> List.filter (fun row -> List.length row > 0)
            |> Grid.ofSeqs

        grid, Guard (Up, guardPos))
