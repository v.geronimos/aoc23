﻿open Vass.Stuff
open aoc24.day6
open aoc24.day6.Domain

let solve = Solver.solve Parser.inputParser part1 part2

solve "example.txt"
solve "input.txt"