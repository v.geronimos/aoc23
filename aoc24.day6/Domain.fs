﻿module aoc24.day6.Domain

open Vass.Stuff
open Vass.Stuff.Types

type Tile = Obstruction | Passable

type Direction = Up | Right | Left | Down
type Position = int * int
type Guard = Guard of Direction * Position

let rotate = function
    | Up -> Right
    | Right -> Down
    | Down -> Left
    | Left -> Up

let move (row, col) = function
    | Up    -> row - 1, col
    | Down  -> row + 1, col
    | Right -> row, col + 1
    | Left  -> row, col - 1

let position (Guard (_, pos)) = pos

let moveGuard (map: Tile grid) (Guard (direction, position)) =
    let forward = (move position direction)
    map.TryGetCell forward
    |> Option.map (function
        | Passable -> Guard (direction, forward)
        | Obstruction -> Guard (rotate direction, position))

let followPath map guard =
    let step guard' =
        let guard'' = moveGuard map guard'
        match guard'' with
        | Some (Guard (dir, pos)) -> Some (Some pos, Guard (dir, pos))
        | None -> None

    (Some (position guard) :: List.unfold step guard)
    |> List.choose id
    |> List.distinct

let isLoop map guard =
    let step guard' =
        moveGuard map guard'
        |> Option.map (fun guard'' -> guard'', guard'')

    Seq.unfold step guard
    |> Seq.scan (fun (visited, _) guard ->
        if (visited |> Set.contains guard)
        then (visited, Some true)
        else ((Set.add guard visited), None)) (Set.empty, None)
    |> Seq.tryPick snd
    |> Option.defaultValue false

let part1 (map, guard) =
    followPath map guard
    |> List.length
    |> Some

let part2 (map, guard) =
    let addObstruction position = map |> Grid.change position Obstruction

    followPath map guard
    |> List.filter (fun obstructionPos -> isLoop (addObstruction obstructionPos) guard)
    |> List.length
    |> Some
