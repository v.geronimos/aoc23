﻿module aoc24.day8.Parser

open Vass.Stuff
open Vass.Stuff.Parser
open Domain

let tile =
    character
    |> choose (function
        | '.' -> Some Empty
        | c when '0' <= c && c <= '9' -> Some (Antenna c)
        | c when 'a' <= c && c <= 'z' -> Some (Antenna c)
        | c when 'A' <= c && c <= 'Z' -> Some (Antenna c)
        | _ -> None)

let row = list tile |> filter (fun row -> List.length row > 0)

let inputParser =
    separatedList newline row
    |> map Grid.ofSeqs
