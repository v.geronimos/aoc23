﻿module aoc24.day8.Domain

open System
open Vass.Stuff
open Vass.Stuff.Types

type Frequency = char
type Tile = Antenna of Frequency | Empty

type Coord = { x: int; y: int }
type Antenna = { frequency: Frequency; position: Coord }

let distance a b = {
    x = b.position.x - a.position.x
    y = b.position.y - a.position.y
}

let antinodes mapSize antennas =
    let frequencies =
        antennas
        |> List.groupBy _.frequency

    frequencies
    |> Seq.collect (fun (_, antennas) ->
        Seq.allPairs antennas antennas
        |> Seq.choose (fun (a, b) ->
            if a.position = b.position
            then None
            else
                let d = distance a b
                Some { x = a.position.x - d.x; y = a.position.y - d.y }
            ))
    |> Seq.distinct
    |> Seq.filter (fun coord -> coord.x >= 0 && coord.y >= 0 && coord.x < mapSize.x && coord.y < mapSize.y)
    |> List.ofSeq

let harmonicAntinodes mapSize antennas =
    let frequencies =
        antennas
        |> List.groupBy _.frequency

    let maxNodes = Math.Min(mapSize.x, mapSize.y)

    frequencies
    |> Seq.collect (fun (_, antennas) ->
        Seq.allPairs antennas antennas
        |> Seq.choose (fun (a, b) ->
            if a.position = b.position
            then None
            else
                let d = distance a b
                Some [ for i in 0 .. maxNodes -> { x = a.position.x - d.x * i; y = a.position.y - d.y * i } ]
            )
        |> Seq.concat)
    |> Seq.distinct
    |> Seq.filter (fun coord -> coord.x >= 0 && coord.y >= 0 && coord.x < mapSize.x && coord.y < mapSize.y)
    |> List.ofSeq

let part1 (input: Tile grid) =
    let antennas =
        input
        |> Grid.foldi (fun acc loc (x, y) ->
            match loc with
            | Antenna frequency -> List.append acc [{ frequency = frequency; position = { x = x; y = y } }]
            | Empty -> acc
        ) []
    antinodes { x = input.Columns; y = input.Rows } antennas
    |> List.length
    |> Some

let part2 input =
    let antennas =
        input
        |> Grid.foldi (fun acc loc (x, y) ->
            match loc with
            | Antenna frequency -> List.append acc [{ frequency = frequency; position = { x = x; y = y } }]
            | Empty -> acc
        ) []
    harmonicAntinodes { x = input.Columns; y = input.Rows } antennas
    |> List.length
    |> Some
