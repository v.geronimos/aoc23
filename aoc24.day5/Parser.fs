﻿module aoc24.day5.Parser

open Domain
open Vass.Stuff
open Vass.Stuff.Parser

let pageNumber = number
let ordering =
    combine3 pageNumber (literal "|") pageNumber
    |> map (fun (a, _, b) -> (a, b))

let orderings = separatedList newline ordering

let update = separatedList (literal ",") pageNumber |> filter (fun pns -> (List.length pns) > 0)
let updates = separatedList newline update

let inputParser =
    combine3 orderings newline updates
    |> map (fun (orderings, _, updates) -> (orderings, updates))
