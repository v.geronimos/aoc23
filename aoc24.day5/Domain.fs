﻿module aoc24.day5.Domain

open Vass.Stuff
open Vass.Stuff.Types

type PageNumber = int64
type Ordering = PageNumber * PageNumber
type Update = PageNumber list

let isSorted (orderings: Ordering list) (updatedPages: Update) =
    let indexOf x = updatedPages |> List.tryFindIndex (fun u -> u = x)
    orderings
    |> List.forall (fun (first, second) ->
        match (indexOf first), (indexOf second) with
        | Some x, Some y when y > x -> true
        | None, _ | _, None -> true
        | _ -> false)

let comparePageOrder (orderings: Ordering list) page1 page2 =
    orderings
    |> List.tryPick (fun (a, b) ->
        if a = page1 && b = page2
        then Some -1
        else if a = page2 && b = page1
        then Some 1
        else None)
    |> Option.defaultValue 0

let middle (xs: 'a list) =
    let idx = (List.length xs) / 2
    List.item idx xs

let part1 (orderings: Ordering list, updates: Update list) =
    updates
    |> List.filter (isSorted orderings)
    |> List.map middle
    |> List.sum
    |> Some

let part2 (orderings: Ordering list, updates: Update list) =
    updates
    |> List.filter (isSorted orderings >> not)
    |> List.map (List.sortWith (comparePageOrder orderings))
    |> List.map middle
    |> List.sum
    |> Some
