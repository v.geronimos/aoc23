﻿module day3.Types
    type Position = {
        columnStart: int
        columnEnd: int
    }

    type Token =
        | Number of int
        | Symbol of char
        | Crap
