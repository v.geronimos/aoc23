﻿open System
open System.IO
open day3.Types

let input = File.ReadAllLines "input.txt" |> List.ofArray
printfn $"Input has %A{input.Length} lines"

let (|Digit|_|) c =
    if (Char.IsDigit c)
    then Some (int(c - '0'))
    else None

let parseChar c =
    match c with
    | Digit d -> Number d
    | '.' -> Crap
    | _ -> Symbol c

let tokenLines =
    input
    |> List.map (fun line ->
        let results, current =
            line
            |> Seq.mapi (fun col c -> (col, c))
            |> Seq.fold (fun (results, current) (column, c) ->
                match current, (parseChar c) with
                | None, Crap -> results, None
                | None, value -> results, Some ({ columnStart = column; columnEnd = column }, value)

                 // This case shouldn't happen
                | Some (_, Crap), value -> results, Some ({ columnStart = column; columnEnd = column }, value)

                | Some (position, value), Crap -> (List.append results [position, value]), None
                | Some (position, value), Symbol s -> (List.append results [position, value; { columnStart = column; columnEnd = column }, Symbol s]), None

                | Some (position, Number cur), Number n -> results, Some ({ position with columnEnd = column}, Number (cur * 10 + n))
                | Some (position, Symbol s), Number n -> (List.append results [position, Symbol s]), Some ({ columnStart = column; columnEnd = column }, Number n)) ([], None)

        match current with
        | None -> results
        | Some token -> (List.append results [token]))

let numbers = tokenLines |> List.collect (fun ts -> ts |> List.filter (function | _, Number n -> true | _ -> false))
printfn $"Found %A{numbers.Length} numbers"

let partNumbers =
    List.concat [[[]]; tokenLines; [[]]]
    |> List.windowed 3
    |> List.collect (fun window ->
        let cur = window[1]
        let around = List.concat window

        cur
        |> List.choose (
            function
            | numberPos, Number n ->
                let isMatch =
                    around
                    |> List.exists (
                        function
                        | symbolPos, Symbol _ -> (numberPos.columnStart - 1) <= symbolPos.columnStart && symbolPos.columnStart <= (numberPos.columnEnd + 1)
                        | _ -> false)
                if isMatch
                then Some n
                else None
            | _ -> None))

printfn $"Found %A{partNumbers.Length} part numbers"

let part1 = partNumbers |> List.sum

printfn $"Part 1 output: %A{part1}"


let gears =
    List.concat[[[]]; tokenLines; [[]]]
    |> List.windowed 3
    |> List.collect (fun window ->
        let cur = window[1]
        let around = List.concat window

        cur
        |> List.choose (
            function
            | symbolPos, Symbol '*' ->
                let partNumbers =
                    around
                    |> List.filter (
                        function
                        | numberPos, Number n -> (numberPos.columnStart - 1) <= symbolPos.columnStart && symbolPos.columnStart <= (numberPos.columnEnd + 1)
                        | _ -> false)

                if partNumbers.Length = 2
                then Some (partNumbers |> List.map snd)
                else None
            | _ -> None
        ))

printfn $"Found %A{gears.Length} gears"

let part2 =
    gears
    |> List.map (
        function
        | [Number a; Number b] -> a * b
        | _ -> 0)
    |> List.sum

printfn $"Part 2 output: %A{part2}"
