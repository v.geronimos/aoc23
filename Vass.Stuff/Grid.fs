﻿module Vass.Stuff.Grid

open Types

let ofSeqs (input: seq<#seq<'a>>) =
    Grid(input |> Seq.map Array.ofSeq |> Array.ofSeq |> array2D)

let transform (sizer: int -> int -> int * int) (initialiser: int -> int -> 'a grid -> int -> int -> 'b) (grid: 'a grid) =
    let rows, cols = sizer grid.Rows grid.Columns
    let dst = Array2D.init rows cols (initialiser rows cols grid)
    Grid dst

let transpose (grid: 'a grid) =
    grid |> transform (fun r c -> c, r) (fun _ _ src i j -> src[j, i])

let revRows (grid: 'a grid) =
    grid |> transform (fun r c -> r, c) (fun r _ src i j -> src[r - 1 - i, j])

let revCols (grid: 'a grid) =
    grid |> transform (fun r c -> r, c) (fun _ c src i j -> src[i, c - 1 - j])

let zip (a: 'a grid) (b: 'b grid): ('a * 'b) grid =
    let rows = min a.Rows b.Rows
    let cols = min a.Columns b.Columns
    let dst = Array2D.init rows cols (fun i j -> a[i, j], b[i, j])
    Grid dst

let fold (folder: 'State -> 'a -> 'State) (state: 'State) (grid: 'a grid) =
    Seq.allPairs [0..grid.Rows-1] [0..grid.Columns-1]
    |> Seq.fold (fun s (r,c) -> folder s grid[r, c]) state

let foldi (folder: 'State -> 'a -> int * int -> 'State) (state: 'State) (grid: 'a grid) =
    Seq.allPairs [0..grid.Rows-1] [0..grid.Columns-1]
    |> Seq.fold (fun s (r,c) -> folder s grid[r, c] (r, c)) state

let forall (predicate: 'a -> bool) = fold (fun s a -> s && (predicate a)) true

let change (row: int, col: int) value (grid: 'a grid) =
    grid
    |> transform (fun r c -> r, c) (fun _ _  src i j ->
        if row = i && col = j
        then value
        else src[i, j])
