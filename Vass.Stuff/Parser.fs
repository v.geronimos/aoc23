﻿namespace Vass.Stuff

open System
open System.Text.RegularExpressions

type 'a ParseFunc = string * int -> ('a * int) option

module Parser =
    let parse (parser: 'a ParseFunc) input =
        match parser (input, 0) with
        | Some (result, _) -> Some result
        | None -> None

    let map (mapper: 'a -> 'b) (parser: 'a ParseFunc): 'b ParseFunc =
        fun (input, index) ->
            parser (input, index)
            |> Option.map (fun (output, index) -> (mapper output), index)

    let choose (chooser: 'a -> 'b option) (parser: 'a ParseFunc): 'b ParseFunc =
        fun (input, index) ->
            parser (input, index)
            |> Option.bind (fun (output, index) -> (chooser output) |> Option.map (fun outputB -> outputB, index))

    let filter (predicate: 'a -> bool) (parser: 'a ParseFunc): 'a ParseFunc =
        fun (input, index) ->
            parser (input, index)
            |> Option.bind (fun (output, index) -> if (predicate output) then Some (output, index) else None)

    let oneOf (parsers: 'a ParseFunc seq): 'a ParseFunc =
        fun (input, index) ->
            parsers
            |> Seq.choose (fun parser -> parser (input, index))
            |> Seq.tryHead

    let optional parser: 'a option ParseFunc =
        oneOf [
            parser |> map Some
            fun (_, index) -> Some (None, index)
        ]

    let not parser: string ParseFunc =
        fun (input, index) ->
            let rec getUnmatches index length =
                if (index + length) >= input.Length
                then
                    if length > 0
                    then Some (input.Substring(index, length - 1), index + length)
                    else None
                else
                    match parser (input, index + length) with
                    | Some _ ->
                        match length with
                        | 0 -> None
                        | _ -> Some (input.Substring(index, length), index + length)
                    | None -> getUnmatches index (length + 1)

            getUnmatches index 0

    let seq (parser: 'a ParseFunc): 'a list ParseFunc =
        fun (input, index) ->
            let list = List.unfold (fun nextIndex ->
                match parser (input, nextIndex) with
                | Some (a, index') -> Some ((a, index'), index')
                | None -> None) index

            match List.tryLast list with
            | Some (_, index) -> Some ((list |> List.map fst), index)
            | _ -> Some ([], index)

    let list (parser: 'a ParseFunc): 'a list ParseFunc = parser |> seq

    let repeat count (parser: 'a ParseFunc) =
        list parser |> filter (List.length >> ((=) count))

    let combine (parserA: 'a ParseFunc) (parserB: 'b ParseFunc): ('a * 'b) ParseFunc =
        fun (input, index) ->
            match parserA (input, index) with
            | Some (a, index) ->
                match parserB (input, index) with
                | Some (b, index) -> Some ((a, b), index)
                | _ -> None
            | _ -> None
    let combine3 (parserA: 'a ParseFunc) (parserB: 'b ParseFunc) (parserC: 'c ParseFunc): ('a * 'b * 'c) ParseFunc =
        fun (input, index) ->
            match parserA (input, index) with
            | Some (a, index) ->
                match parserB (input, index) with
                | Some (b, index) ->
                    match parserC (input, index) with
                    | Some (c, index) -> Some ((a, b, c), index)
                    | _ -> None
                | _ -> None
            | _ -> None
    let combine4 (parserA: 'a ParseFunc) (parserB: 'b ParseFunc) (parserC: 'c ParseFunc) (parserD: 'd ParseFunc): ('a * 'b * 'c * 'd) ParseFunc =
        fun (input, index) ->
            match parserA (input, index) with
            | Some (a, index) ->
                match parserB (input, index) with
                | Some (b, index) ->
                    match parserC (input, index) with
                    | Some (c, index) ->
                        match parserD (input, index) with
                        | Some (d, index) -> Some ((a, b, c, d), index)
                        | _ -> None
                    | _ -> None
                | _ -> None
            | _ -> None
    let combine5 (parserA: 'a ParseFunc) (parserB: 'b ParseFunc) (parserC: 'c ParseFunc) (parserD: 'd ParseFunc) (parserE: 'e ParseFunc): ('a * 'b * 'c * 'd * 'e) ParseFunc =
        fun (input, index) ->
            match parserA (input, index) with
            | Some (a, index) ->
                match parserB (input, index) with
                | Some (b, index) ->
                    match parserC (input, index) with
                    | Some (c, index) ->
                        match parserD (input, index) with
                        | Some (d, index) ->
                            match parserE (input, index) with
                            | Some (e, index) -> Some ((a, b, c, d, e), index)
                            | _ -> None
                        | _ -> None
                    | _ -> None
                | _ -> None
            | _ -> None

    let literal (s: string): string ParseFunc =
        let length = s.Length

        fun (input, index) ->
            if input.Substring(index).StartsWith(s)
            then Some (s, index + length)
            else None

    let character: char ParseFunc =
        fun (input, index) ->
            if index < input.Length
            then Some (input.Chars(index), index + 1)
            else None

    let stringMatching (pattern: string): string ParseFunc =
        let r = Regex(pattern)
        fun (input, index) ->
            input
            |> Seq.skip index
            |> Seq.scan (fun acc c -> $"{acc}{c}") ""
            |> Seq.takeWhile r.IsMatch
            |> Seq.tryLast
            |> Option.map (fun str -> str, index + str.Length)

    let number32: int ParseFunc =
        fun (input, index) ->
            let n, count = input
                         |> Seq.skip index
                         |> Seq.takeWhile (fun x -> '0' <= x && x <= '9')
                         |> Seq.fold (fun (acc, count) n -> (acc * 10 + int(n - '0'), count + 1)) (0, 0)
            if count = 0
            then None
            else Some (n, index + count)

    let number: int64 ParseFunc =
        fun (input, index) ->
            let n, count = input
                         |> Seq.skip index
                         |> Seq.indexed
                         |> Seq.takeWhile (fun (i, x) -> (i = 0 && x = '-') || '0' <= x && x <= '9')
                         |> Seq.map (snd >> Char.ToString)
                         |> Seq.fold (+) ""
                         |> (fun s ->
                             let success, value = Int64.TryParse s
                             match success, value with
                             | true, number -> number, s.Length
                             | false, _ -> 0, 0)
            if count = 0
            then None
            else Some (n, index + count)

    let literalMap (inputMap: Map<string, 'a>): 'a ParseFunc =
        let parsers =
            inputMap
            |> Map.toSeq
            |> Seq.map (fun (k, v) -> literal k |> map (fun _ -> v))

        oneOf parsers

    let whitespace = seq (literal " ") |> map (fun _ -> ())
    let newline = oneOf [(literal "\n"); (literal "\r\n")] |> map (fun _ -> ())

    let separatedSeq (separator: 'sep ParseFunc) (parser: 'a ParseFunc): 'a seq ParseFunc =
        let separatorParser = (combine3 whitespace separator whitespace)

        fun (input, index) ->
            let rec getMatches ((acc: 'a seq), index) =
                match parser (input, index) with
                | Some (a, index) ->
                    match separatorParser (input, index) with
                    | Some (_, index) -> getMatches ((Seq.append acc [a]), index)
                    | _ -> Some ((Seq.append acc [a]), index)
                | None -> Some (acc, index)

            getMatches ([], index)

    let separatedList (separator: 'sep ParseFunc) (parser: 'a ParseFunc): 'a list ParseFunc = separatedSeq separator parser |> map List.ofSeq
