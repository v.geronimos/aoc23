﻿namespace Vass.Stuff

open System.Diagnostics
open System.IO

type SolveFunc<'a, 'b> = 'a -> 'b option

module Solver =

    let notDone _ = None

    let solve (inputParser: 'a ParseFunc) (part1Solver: SolveFunc<'a, int64>) (part2Solver: SolveFunc<'a, int64>) file =
        printfn $"Parsing file %s{file}..."

        let parsedInput =
            match (File.ReadAllText file |> Parser.parse inputParser) with
            | Some stuff -> stuff
            | None -> raise (exn $"could not read %A{file}")

        let showResult (partNo: int) solver =
            let stopwatch = Stopwatch ()
            stopwatch.Start()
            let output = solver parsedInput
            stopwatch.Stop()

            match output with
            | Some x -> printfn $"Part {partNo} output: %d{x} (in %A{stopwatch.Elapsed})"
            | None -> printfn $"Part {partNo} not implemented"

        printfn $"Solving file %s{file}..."

        showResult 1 part1Solver
        showResult 2 part2Solver

        printfn ""
