﻿module aoc24.day9.Parser

open System
open Vass.Stuff
open Vass.Stuff.Parser
open Domain

let digit =
    character
    |> choose (function
        | c when '0' <= c && c <= '9' -> Some (int (c - '0'))
        | _ -> None)
let digits = list digit

let disk =
    digits
    |> map (fun ds ->
        ds
        |> List.mapi (fun i d ->
            let item =
                if i % 2 = 1
                then Empty
                else File (int64 (i / 2))

            { offset = i; length = d; blockType = item })
        |> Array.ofList)

let inputParser = combine disk newline |> map fst
