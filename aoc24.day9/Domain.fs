﻿module aoc24.day9.Domain

open Microsoft.FSharp.Collections
open Vass.Stuff
open Vass.Stuff.Types

type Block =
    | Empty
    | File of int64

type Node = {
    blockType: Block
    offset: int64
    length: int
}

let toDiskMap nodes =
    nodes
    |> Seq.map (fun node -> List.replicate node.length node.blockType)
    |> Seq.concat
    |> Array.ofSeq

let defrag nodes =
    let diskMap = toDiskMap nodes
    let size = Array.length diskMap

    Seq.unfold (fun (front, back) ->
        if front >= size
        then None
        else if front >= back
        then Some (Empty, (front + 1, back))
        else
            let currentItem = diskMap |> Array.item front
            match currentItem with
            | File _ -> Some (currentItem, (front + 1, back))
            | Empty ->
                let backIdx =
                    diskMap
                    |> Array.truncate back
                    |> Array.tryFindIndexBack (function
                        | File _ -> true
                        | Empty -> false)
                match backIdx with
                | Some i when i > front ->
                    let backFile = diskMap |> Array.item i
                    Some (backFile, (front + 1, i))
                | _ -> Some (Empty, (front + 1, front))
                ) (0, size)
    |> Array.ofSeq

let lessCrappyDefrag nodes =
    let filesRev =
        nodes
        |> Array.filter (function
            | { blockType = File _ } -> true
            | _ -> false)
        |> Array.rev

    let movableNodes currentOffset size alreadyMoved =
        filesRev
        |> Seq.scan (fun state node ->
            if node.offset < currentOffset
            then None
            else
                state
                |> Option.map (fun (movedSize, movedNodes) ->
                    match node.blockType with
                    | Empty -> movedSize, movedNodes
                    | File f ->
                        let fits = node.length <= (size - movedSize)
                        let fileAlreadyMoved = alreadyMoved |> Set.contains f

                        if fileAlreadyMoved || not(fits)
                        then movedSize, movedNodes
                        else movedSize + node.length, node :: movedNodes
                )
        ) (Some (0, []))
        |> Seq.takeWhile (function
            | Some _ -> true
            | None -> false)
        |> Seq.map Option.get
        |> Seq.last
        |> (snd >> List.rev)

    nodes
    |> Array.fold (fun (defragged: Node list, movedFiles: Set<int64>) (node: Node) ->
        match node.blockType with
        | File f ->
            let alreadyMoved = movedFiles |> Set.contains f
            if alreadyMoved
            then ((List.append defragged [{ blockType = Empty; offset = node.offset; length = node.length }] ), movedFiles)
            else ((List.append defragged [node]), movedFiles |> Set.add f)
        | Empty ->
            let nodesToMove = movableNodes node.offset node.length movedFiles
            let movedFiles =
                nodesToMove
                |> Seq.choose (function
                    | { blockType = File f } -> Some f
                    | _ -> None)
                |> Set.ofSeq
                |> Set.union movedFiles

            let leftoverSpace = node.length - (nodesToMove |> List.sumBy _.length)
            if leftoverSpace > 0
            then
                let emptyOffset = node.offset + int64 (node.length - leftoverSpace)
                let emptyNode = { blockType = Empty; offset = emptyOffset; length = leftoverSpace }
                let newNodes = List.append nodesToMove [emptyNode]
                ((List.append defragged newNodes), movedFiles)
            else
                ((List.append defragged nodesToMove), movedFiles)
    ) ([], Set.empty)
    |> fst
    |> Array.ofList

let checksum diskMap =
    diskMap
    |> Seq.mapi (fun i block ->
        match block with
        | File id -> (int64 i) * id
        | _ -> 0L)
    |> Seq.reduce (+)

let part1 input =
    defrag input
    |> checksum
    |> Some

let part2 input =
    lessCrappyDefrag input
    |> toDiskMap
    |> checksum
    |> Some
