﻿module aoc24.day12.Parser

open Vass.Stuff
open Vass.Stuff.Parser
open Domain

let plant =
    character
    |> choose (function
        | c when 'A' <= c && c <= 'Z' -> Some (Plant c)
        | _ -> None)

let row = list plant |> filter (fun row -> List.length row > 0)

let inputParser =
    separatedList newline row
    |> map Grid.ofSeqs
