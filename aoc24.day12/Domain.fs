﻿module aoc24.day12.Domain

open System
open Vass.Stuff
open Vass.Stuff.Types

type Plant = Plant of char
type Region = {
    plant: Plant
    plots: (int * int) list
}

type Side = North | East | South | West
type Fence = Fence of Side * int * int * int

let isAdjacent (r1, c1) (r2, c2) =
    match (r2 - r1), (c2 - c1) with
    | -1, 0 | 1, 0 | 0, -1 | -1, 0 -> true
    | _ -> false

let boundingBox points =
    let r1 = points |> List.minBy fst |> fst
    let r2 = points |> List.maxBy fst |> fst
    let c1 = points |> List.minBy snd |> snd
    let c2 = points |> List.maxBy snd |> snd

    r1, r2, c1, c2

let isOverlapping (n1, length1) (n2, length2) =
    (n1 <= n2 && n2 <= n1 + length1) ||
    (n2 <= n1 && n1 <= n2 + length2)

let isDirectionalOverlap (r1, c1, length1) (r2, c2, length2) = function
    | North | South -> r1 = r2 && isOverlapping (c1, length1) (c2, length2)
    | East | West -> c1 = c2 && isOverlapping (r1, length1) (r2, length2)

let directionalMerge (extents: (int * int * int) list) = function
    | North | South ->
        let r = extents |> List.item 0 |> (fun (r, _, _) -> r)
        let cStart = extents |> Seq.map (fun (_, c, _) -> c) |> Seq.min
        let cEnd = extents |> Seq.map (fun (_, c, length) -> c + length) |> Seq.max
        r, cStart, (cEnd - cStart)
    | East | West ->
        let c = extents |> List.item 0 |> (fun (_, c, _) -> c)
        let rStart = extents |> Seq.map (fun (r, _, _) -> r) |> Seq.min
        let rEnd = extents |> Seq.map (fun (r, _, length) -> r + length) |> Seq.max
        rStart, c, (rEnd - rStart)

let mergeRegions (regions: Region seq) =
    regions
    |> Seq.fold (fun mergedRegions region ->
        let adjacentRegions, nonAdjacentRegions =
            mergedRegions
            |> List.partition (fun r ->
                Seq.allPairs region.plots r.plots
                |> Seq.exists (fun (a, b) -> isAdjacent a b)
                )

        if List.length adjacentRegions = 0
        then region :: nonAdjacentRegions
        else
            let newRegion = {
                plant = region.plant
                plots =
                    adjacentRegions
                    |> List.collect _.plots
                    |> List.append region.plots
            }
            newRegion :: nonAdjacentRegions
        ) []

let mergeFences (fences: Fence seq) =
    fences
    |> Seq.fold (fun mergedFences (Fence (dir1, r1, c1, length1)) ->
        let adjacentFences, nonAdjacentFences =
            mergedFences
            |> List.partition (fun (Fence (dir2, r2, c2, length2)) ->
                if dir1 <> dir2
                then false
                else isDirectionalOverlap (r1, c1, length1) (r2, c2, length2) dir1
                )

        if List.length adjacentFences = 0
        then (Fence (dir1, r1, c1, length1)) :: nonAdjacentFences
        else
            let r', c', length' =
                adjacentFences
                |> List.map (fun (Fence (_,r,c,length)) -> (r,c,length))
                |> List.append [ r1, c1, length1 ]
                |> directionalMerge
                <| dir1
            (Fence (dir1, r', c', length')) :: nonAdjacentFences
        ) []

let regionate (plants: Plant grid) =
    Seq.allPairs [0..plants.Rows-1] [0..plants.Columns-1]
    |> Seq.map (fun (r, c) -> { plant = plants[r, c]; plots = [r,c] })
    |> List.ofSeq
    |> List.groupBy _.plant
    |> List.collect (snd >> mergeRegions)

let fence region =
    region.plots
    |> List.fold (fun acc (r, c) ->
        let borders =
            seq { North, -1, 0; South, 1, 0; West, 0, -1; East, 0, 1 }
            |> Seq.choose (fun (dir, r', c') ->
                match region.plots |> List.contains (r + r', c + c') with
                | true -> None
                | false -> Some (Fence (dir, r, c, 1))
                )
            |> List.ofSeq

        List.append borders acc
        ) []

let perimeterLength region =
    region
    |> fence
    |> List.length
    |> int64

let sideCount region =
    region
    |> fence
    |> mergeFences
    |> List.length
    |> int64

let area region = region.plots |> List.length |> int64

let part1 (input: Plant grid) =
    regionate input
    |> List.sumBy (fun region -> (perimeterLength region) * (area region))
    |> Some

let part2 (input: Plant grid) =
    regionate input
    |> List.sumBy (fun region -> (sideCount region) * (area region))
    |> Some
