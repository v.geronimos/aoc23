﻿open Vass.Stuff
open aoc24.day12
open aoc24.day12.Domain

let solve = Solver.solve Parser.inputParser part1 part2

solve "example.txt"
solve "example2.txt"
solve "example3.txt"
solve "example4.txt"
solve "example5.txt"
solve "input.txt"
