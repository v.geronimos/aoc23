﻿open System.IO
open Vass.Stuff
open aoc24.day1.Domain

let solve file =
    printfn $"Solving file %s{file}..."

    let locations =
        match (File.ReadAllText file |> Parser.parse aoc24.day1.Parser.inputParser) with
        | Some stuff -> stuff
        | None -> raise (exn $"could not read %A{file}")

    let part1 = distance (fst locations) (snd locations)

    printfn $"Part 1 output: %A{part1}"

    let part2 = similarity (fst locations) (snd locations)

    printfn $"Part 2 output: %A{part2}"

solve "example.txt"
solve "input.txt"
