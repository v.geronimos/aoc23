﻿module aoc24.day1.Parser

open Vass.Stuff
open Vass.Stuff.Parser
open aoc24.day1.Domain

let locationId = number |> map LocationId
let pair =
   combine3 locationId whitespace locationId
   |> map (fun (l, _, r) -> (l, r))

let inputParser =
    separatedList newline pair
    |> map List.unzip
