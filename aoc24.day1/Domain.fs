﻿module aoc24.day1.Domain

open System

type LocationId = LocationId of int64

let distance xs ys =
    let sortedXs = xs |> List.sortBy (fun (LocationId id) -> id)
    let sortedYs = ys |> List.sortBy (fun (LocationId id) -> id)

    List.zip sortedXs sortedYs
    |> List.fold (fun acc (LocationId x, LocationId y) -> acc + Math.Abs(x - y)) 0L

let similarity xs ys =
    let valueCounts =
        ys
        |> List.groupBy id
        |> Map.ofList
        |> Map.map (fun _ items -> items |> List.length)

    xs
    |> List.choose (fun x -> valueCounts |> Map.tryFind x |> Option.map (fun count -> x, int64 count ))
    |> List.fold (fun acc (LocationId x, count) -> acc + x * count) 0L
