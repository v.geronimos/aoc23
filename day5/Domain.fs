﻿module day5.Domain

open System
open Microsoft.FSharp.Core

type TokenType =
    | Seed
    | Soil
    | Fertiliser
    | Water
    | Light
    | Temperature
    | Humidity
    | Location

type Seed<'a> = Seed of 'a
type Soil<'a> = Soil of 'a
type Fertiliser<'a> = Fertiliser of 'a
type Water<'a> = Water of 'a
type Light<'a> = Light of 'a
type Temperature<'a> = Temperature of 'a
type Humidity<'a> = Humidity of 'a
type Location<'a> = Location of 'a

type RangeMapEntry = {
    sourceStart: int64
    destinationStart: int64
    length: int64
}
type RangeMap = {
    source: TokenType
    destination: TokenType
    entries: RangeMapEntry list
}

type Range = {
    start: int64
    length: int64
}

let isInRange first length value =
    let last = first + length - 1L
    first <= value && value <= last

let rangesOverlap rangeA rangeB =
    let lastA = rangeA.start + rangeA.length - 1L
    let lastB = rangeB.start + rangeB.length - 1L

    rangeA.start <= lastB && rangeB.start <= lastA

let fillGaps rangeEntries =
    let gaps =
        (List.append rangeEntries [{ sourceStart = 0; destinationStart = 0; length = 0 }])
        |> List.choose (fun entry ->
            let nextNumber = entry.sourceStart + entry.length
            let nextEntry =
                rangeEntries
                |> List.filter (fun e -> e.sourceStart >= nextNumber)
                |> List.append [{ sourceStart = Int64.MaxValue; destinationStart = Int64.MaxValue; length = 1 }]
                |> List.minBy (fun e -> e.sourceStart)

            if nextEntry.sourceStart = nextNumber
            then None
            else Some { sourceStart = nextNumber; destinationStart = nextNumber; length = nextEntry.sourceStart - nextNumber })

    List.append rangeEntries gaps
    |> List.sortBy (fun e -> e.sourceStart)

let mapValueToRange rangeMap =
    let filledRangeMap = fillGaps rangeMap

    fun sourceValue ->
        filledRangeMap
        |> List.pick (fun entry ->
            let offset = sourceValue - entry.sourceStart
            if 0L <= offset && offset < entry.length
            then Some (entry.destinationStart + offset)
            else None)

let createValueMapper source destination rangeMaps =
    let rangeMap = rangeMaps |> List.find (fun rm -> rm.source = source && rm.destination = destination)
    mapValueToRange rangeMap.entries

let union rangeA rangeB =
    let nextA = rangeA.start + rangeA.length
    let nextB = rangeB.start + rangeB.length

    if isInRange rangeA.start rangeA.length rangeB.start
    then Some { rangeA with length = (max nextA nextB) - rangeA.start }
    else if isInRange rangeB.start rangeB.length rangeA.start
    then Some { rangeB with length = (max nextA nextB) - rangeB.start }
    else None

let mapRangeToRanges rangeMap =
    let filledMap = fillGaps rangeMap

    fun sourceRange ->
        let sourceLast = sourceRange.start + sourceRange.length - 1L

        filledMap
        |> List.fold (fun (first, results) entry ->
            let entryLast = entry.sourceStart + entry.length - 1L

            if entryLast < first || sourceLast < entry.sourceStart
            then (first, results)
            else
                let last = min entryLast sourceLast
                let offset = first - entry.sourceStart
                let mappedRange = { start = entry.destinationStart + offset; length = last - entry.sourceStart + 1L - offset }

                let newResults = mappedRange :: results
                let newFirst = last + 1L
                (newFirst, newResults)) (sourceRange.start, [])
        |> snd
        |> List.sortBy (fun r -> r.start)

let createRangeMapper source destination rangeMaps =
    let rangeMap = rangeMaps |> List.find (fun rm -> rm.source = source && rm.destination = destination)
    mapRangeToRanges rangeMap.entries
