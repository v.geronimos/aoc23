﻿module day5.Parser
open Vass.Stuff.Parser

open day5.Domain

let seeds =
    combine (literal "seeds: ") (separatedSeq whitespace number)
    |> map (fun (_, numbers) -> numbers |> Seq.map Seed |> List.ofSeq)

let tokenType = literalMap (Map.ofList [
    "seed", TokenType.Seed
    "soil", TokenType.Soil
    "fertilizer", TokenType.Fertiliser
    "water", TokenType.Water
    "light", TokenType.Light
    "temperature", TokenType.Temperature
    "humidity", TokenType.Humidity
    "location", TokenType.Location
])

let resultMapHeader =
    combine4 tokenType (literal "-to-") tokenType (combine whitespace (literal "map:"))
    |> map (fun (s,_,d,_) -> s, d)

let resultMapEntry =
    combine5 number whitespace number whitespace number
    |> map (fun (dest, _, source, _, length) -> { destinationStart = dest; sourceStart = source; length = length })

let resultMap =
    combine3 resultMapHeader newline (separatedSeq newline resultMapEntry)
    |> map (fun ((source, dest), _, entries) -> { source = source; destination = dest; entries = entries |> List.ofSeq |> List.sortBy (fun e -> e.sourceStart) })

let resultMaps =
    separatedSeq (seq newline) resultMap
    |> map List.ofSeq

let inputParser =
    combine3 seeds (seq newline) resultMaps
    |> map (fun (seeds, _, resultMaps) -> seeds, resultMaps)
