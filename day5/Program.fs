﻿open System.IO
open Microsoft.FSharp.Collections
open Vass.Stuff
open day5.Domain

let seeds, rangeMaps =
    match (
        File.ReadAllText "input.txt"
        |> Parser.parse day5.Parser.inputParser) with
    | Some stuff -> stuff
    | None -> raise (exn("could not read input"))

let seedToSoil =
    let mapper = createValueMapper TokenType.Seed TokenType.Soil rangeMaps
    fun (Seed seed) -> Soil (mapper seed)
let soilToFertiliser =
    let mapper = createValueMapper TokenType.Soil TokenType.Fertiliser rangeMaps
    fun (Soil soil) -> Fertiliser (mapper soil)
let fertiliserToWater =
    let mapper = createValueMapper TokenType.Fertiliser TokenType.Water rangeMaps
    fun (Fertiliser soil) -> Water (mapper soil)
let waterToLight =
    let mapper = createValueMapper TokenType.Water TokenType.Light rangeMaps
    fun (Water soil) -> Light (mapper soil)
let lightToTemperature =
    let mapper = createValueMapper TokenType.Light TokenType.Temperature rangeMaps
    fun (Light soil) -> Temperature (mapper soil)
let temperatureToHumidity =
    let mapper = createValueMapper TokenType.Temperature TokenType.Humidity rangeMaps
    fun (Temperature soil) -> Humidity (mapper soil)
let humidityToLocation =
    let mapper = createValueMapper TokenType.Humidity TokenType.Location rangeMaps
    fun (Humidity soil) -> Location (mapper soil)


let part1 =
    seeds
    |> List.map (fun seed -> seed, seed |> (seedToSoil >> soilToFertiliser >> fertiliserToWater >> waterToLight >> lightToTemperature >> temperatureToHumidity >> humidityToLocation))
    |> List.minBy snd
    |> snd

printfn $"Part 1 output: %A{part1}"

let seedRangeToSoilRanges =
    let mapper = createRangeMapper TokenType.Seed TokenType.Soil rangeMaps
    fun (Seed seed) -> (mapper seed) |> List.map Soil
let soilRangeToFertiliserRanges =
    let mapper = createRangeMapper TokenType.Soil TokenType.Fertiliser rangeMaps
    fun (Soil soil) -> (mapper soil) |> List.map Fertiliser
let fertiliserRangeToWaterRanges =
    let mapper = createRangeMapper TokenType.Fertiliser TokenType.Water rangeMaps
    fun (Fertiliser soil) -> (mapper soil) |> List.map Water
let waterRangeToLightRanges =
    let mapper = createRangeMapper TokenType.Water TokenType.Light rangeMaps
    fun (Water soil) -> (mapper soil) |> List.map Light
let lightRangeToTemperatureRanges =
    let mapper = createRangeMapper TokenType.Light TokenType.Temperature rangeMaps
    fun (Light soil) -> (mapper soil) |> List.map Temperature
let temperatureRangeToHumidityRanges =
    let mapper = createRangeMapper TokenType.Temperature TokenType.Humidity rangeMaps
    fun (Temperature soil) -> (mapper soil) |> List.map Humidity
let humidityRangeToLocationRanges =
    let mapper = createRangeMapper TokenType.Humidity TokenType.Location rangeMaps
    fun (Humidity soil) -> (mapper soil) |> List.map Location

let seedRanges =
    seeds
    |> List.mapi (fun id (Seed n) -> id, n)
    |> List.partition (fun (id, _) -> id % 2 = 0)
    |> (fun (evens, odds) -> List.zip evens odds)
    |> List.map (fun (start, length) -> Seed { start = snd start; length = snd length })

printfn $"Seed ranges: %A{seedRanges}"

let soils =
    seedRanges
    |> List.collect seedRangeToSoilRanges
printfn "Ranges:"
soils |> List.iter (printfn "%A")

let locations =
    seedRanges
    |> List.collect seedRangeToSoilRanges
    |> List.collect soilRangeToFertiliserRanges
    |> List.collect fertiliserRangeToWaterRanges
    |> List.collect waterRangeToLightRanges
    |> List.collect lightRangeToTemperatureRanges
    |> List.collect temperatureRangeToHumidityRanges
    |> List.collect humidityRangeToLocationRanges
    |> List.sortBy (fun (Location range) -> range.start)

//printfn "Ranges:"
//locations |> List.iter (printfn "%A")

let part2 = locations |> List.head |> (fun (Location range) -> range.start)
printfn $"Part 2 output: %A{part2}"
