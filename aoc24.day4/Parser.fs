﻿module aoc24.day4.Parser

open Domain
open Vass.Stuff
open Vass.Stuff.Parser

let letter = literalMap (Map.ofList [
    "X", X
    "M", M
    "A", A
    "S", S
])

let inputParser =
    separatedList newline (list letter)
    |> map (List.filter (fun row -> List.length row > 0))
    |> map Grid.ofSeqs
