﻿module aoc24.day4.Domain

open Vass.Stuff
open Vass.Stuff.Types

type Letter = X | M | A | S
type WordSearch = Letter grid

type Direction =
    | UpLeft   | Up   | UpRight
    | Left            | Right
    | DownLeft | Down | DownRight

let gridOffset = function
    | UpLeft ->    (-1, -1)
    | Up ->        (-1,  0)
    | UpRight ->   (-1,  1)
    | Left ->      ( 0, -1)
    | Right ->     ( 0,  1)
    | DownLeft ->  ( 1, -1)
    | Down ->      ( 1,  0)
    | DownRight -> ( 1,  1)

let perpendicularDirections = function
    | UpLeft ->    [UpRight; DownLeft ]
    | Up ->        [Left;    Right    ]
    | UpRight ->   [UpLeft;  DownRight]
    | Left ->      [Up;      Down     ]
    | Right ->     [Up;      Down     ]
    | DownLeft ->  [UpLeft;  DownRight]
    | Down ->      [Left;    Right    ]
    | DownRight -> [UpRight; DownLeft ]

let getLetter (wordSearch: WordSearch) (startRow, startCol) (rowOffset, colOffset) index =
    let row = startRow + rowOffset * index
    let col = startCol + colOffset * index
    wordSearch.TryGetCell(row, col)

let isXmas (wordSearch: WordSearch) (startRow, startCol) direction =
    let offsets = gridOffset direction
    let getLetter = getLetter wordSearch (startRow, startCol) offsets

    let x = getLetter 0
    let m = getLetter 1
    let a = getLetter 2
    let s = getLetter 3

    match x, m, a, s with
    | Some X, Some M, Some A, Some S -> true
    | _ -> false

let isXOfMas (wordSearch: WordSearch) (startRow, startCol) direction =
    let isMas direction =
        let offsets = gridOffset direction
        let getLetter = getLetter wordSearch (startRow, startCol) offsets

        let m = getLetter -1
        let a = getLetter  0
        let s = getLetter  1

        match m, a, s with
        | Some M, Some A, Some S -> true
        | _ -> false

    if isMas direction
    then perpendicularDirections direction |> List.exists isMas
    else false

let isMatch matcher directions (wordSearch: WordSearch) (startRow, startCol) =
    directions
    |> List.filter (matcher wordSearch (startRow, startCol))

let part1 (wordSearch: WordSearch) =
    let directions = [UpLeft; Up; UpRight; Left; Right; DownLeft; Down; DownRight]
    wordSearch
    |> Grid.foldi (fun state _ (r,c) ->
        isMatch isXmas directions wordSearch (r, c)
        |> List.length
        |> int64
        |> ((+) state)) 0L
    |> Some

let part2 (wordSearch: WordSearch) =
    let directions = [UpLeft; DownRight]
    wordSearch
    |> Grid.foldi (fun state _ (r,c) ->
        isMatch isXOfMas directions wordSearch (r, c)
        |> List.length
        |> int64
        |> ((+) state)) 0L
    |> Some
