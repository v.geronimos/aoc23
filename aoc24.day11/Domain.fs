﻿module aoc24.day11.Domain

open System
open Vass.Stuff
open Vass.Stuff.Types

type Stone = Stone of int64
type ResultCacheEntry = {
    stones: Stone array
    depthCounts: Map<int64, int64>
}

let (|EvenDigits|_|) (Stone number) =
    let dnumber = double number
    let digits = 1.0 + Math.Floor (Math.Log10 dnumber)
    if digits % 2.0 = 0
    then
        let split = Math.Pow(10, digits / 2.0)
        let left = Math.Floor (dnumber / split)
        let right = dnumber % split
        Some (int64 left, int64 right)
    else
        None

let blinkAt = function
    | Stone 0L -> [| Stone 1L |]
    | EvenDigits (left, right) -> [| Stone left; Stone right |]
    | Stone x -> [| Stone (x * 2024L) |]

let countAfterBlinkingAt times stones =
    let addCacheEntry stone resultCache =
        match resultCache |> Map.tryFind stone with
        | Some _ -> resultCache
        | None ->
            let result = blinkAt stone
            let cacheEntry = {
                stones = result
                depthCounts = Map.empty |> Map.add 1L (Array.length result)
            }
            resultCache |> Map.add stone cacheEntry

    let rec updateCacheEntryCounts depth stone resultCache =
        match depth with
        | 0 -> resultCache
        | _ ->
            let resultCache' = resultCache |> addCacheEntry stone
            let cacheEntry = resultCache' |> Map.find stone
            match cacheEntry.depthCounts |> Map.tryFind depth with
            | Some _ -> resultCache'
            | None ->
                let depth' = depth - 1
                let updatedCache =
                    cacheEntry.stones
                    |> Array.fold (fun resultCache' stone' -> updateCacheEntryCounts depth' stone' resultCache') resultCache'

                let count =
                    cacheEntry.stones
                    |> Array.fold (fun acc stone' ->
                        let cacheEntry' = updatedCache |> Map.find stone'
                        let depth' = cacheEntry'.depthCounts |> Map.find depth'
                        acc + depth') 0L

                let updatedEntry = {
                    stones = cacheEntry.stones
                    depthCounts = cacheEntry.depthCounts |> Map.add depth count
                }

                updatedCache |> Map.add stone updatedEntry

    stones
    |> Array.fold (fun (count, resultCache) stone ->
        let resultCache' =
            resultCache
            |> updateCacheEntryCounts times stone

        let cacheEntry = resultCache' |> Map.find stone
        let count' = cacheEntry.depthCounts |> Map.find times
        (count + count', resultCache')
    ) (0L, Map.empty)
    |> fst

let part1 input =
    input
    |> countAfterBlinkingAt 25
    |> Some

let part2 input =
    input
    |> countAfterBlinkingAt 75
    |> Some
