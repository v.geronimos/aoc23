﻿module aoc24.day11.Parser

open Vass.Stuff
open Vass.Stuff.Parser
open Domain

let stone = number |> map Stone

let inputParser = separatedSeq whitespace stone |> map Array.ofSeq
