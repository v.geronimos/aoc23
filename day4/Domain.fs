﻿module day4.Domain
open System

type Card = {
    id: int
    winners: int list
    numbers: int list
}

let winners card =
    card.winners
    |> List.filter (fun winner ->
        card.numbers
        |> List.contains winner)

let score card =
    match (winners card).Length with
    | 0 -> 0
    | n -> int(Math.Pow(2, float(n - 1)))
