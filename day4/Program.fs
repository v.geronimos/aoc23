﻿open System.IO
open Vass.Stuff

open day4.Domain

let input = File.ReadAllLines "input.txt"
printfn $"Input has %A{input.Length} lines"

let cards =
    input
    |> Array.choose (Parser.parse day4.Parser.card)
printfn $"Parsed %A{cards.Length} cards"

let scores =
    cards
    |> Array.map (fun card -> card.id, (score card))

let part1 = scores |> Array.map snd |> Array.sum
printfn $"Part 1 output: %A{part1}"

let cardsWithDuplicateCounts =
    cards
    |> Array.mapFold (fun (state: (int * int) list) card ->
        let currentCardCopies =
            1 + (state
            |> List.mapi (fun i x -> i, x)
            |> List.filter (fun (i, (_, winners)) -> i < winners)
            |> List.map (fun (_, (copies, _)) -> copies)
            |> List.sum)

        let currentCardWinners = (winners card).Length

        currentCardCopies, ((currentCardCopies, currentCardWinners) :: state)
        ) []
    |> fst

let part2 = cardsWithDuplicateCounts |> Array.sum
printfn $"Part 2 output: %A{part2}"
