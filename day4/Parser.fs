﻿module day4.Parser

open Vass.Stuff
open day4.Domain

let numberSeparator =
    Parser.combine3 Parser.whitespace (Parser.literal "|") Parser.whitespace
    |> Parser.map (fun _ -> ())

let numberList = Parser.seq (Parser.combine Parser.whitespace Parser.number32 |> Parser.map snd)
let cardContents =
    Parser.combine3 numberList numberSeparator numberList
    |> Parser.map (fun (ws, _, ns) -> (List.ofSeq ws), (List.ofSeq ns))

let cardId =
    Parser.combine3 (Parser.combine (Parser.literal "Card") Parser.whitespace) Parser.number32 (Parser.combine (Parser.literal ":") Parser.whitespace)
    |> Parser.map (fun (_, id, _) -> id)

let card =
    Parser.combine cardId cardContents
    |> Parser.map (fun (id, (ws, ns)) -> { id = id; winners = ws; numbers = ns })
