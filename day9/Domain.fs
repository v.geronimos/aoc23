﻿module day9.Domain

type History = int64 list

let deltas (history: History) =
    history |> List.pairwise |> List.map (fun (a, b) -> b - a)

let extrapolate history =
    let rec core (hs: History) l =
        if (hs.Length = 0) then 0L
        else
            let ds = deltas hs
            let l' = (hs |> List.last)
            if (ds |> List.forall ((=) 0L)) then
                l + l'
            else
                core ds (l + l')

    core history 0L
