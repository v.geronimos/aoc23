﻿module day9.Parser

open Vass.Stuff.Parser
open day9.Domain

let history = separatedList whitespace number

let inputParser = separatedList newline history
