﻿open System.IO
open Vass.Stuff
open day9.Domain

let solve file =
    printfn $"Solving file %s{file}..."

    let histories =
        match (File.ReadAllText file |> Parser.parse day9.Parser.inputParser) with
        | Some stuff -> stuff
        | None -> raise (exn ($"could not read %A{file}"))

    let extrapolations =
        histories
        |> List.map extrapolate

    let part1 =
        extrapolations
        |> List.sum

    printfn $"Part 1 output: %A{part1}"

    let extrapolations' =
        histories
        |> List.map (List.rev >> extrapolate)

    let part2 =
        extrapolations'
        |> List.sum

    printfn $"Part 2 output: %A{part2}"

solve "example.txt"
solve "input.txt"
