﻿module aoc24.day7.Parser

open Vass.Stuff
open Vass.Stuff.Parser
open Domain

let result = number
let values = separatedList whitespace number
let equation =
    combine3 result (literal ": ") values
    |> map (fun (r, _, vs) -> { result = r; values = vs })

let inputParser =
    separatedList newline equation
