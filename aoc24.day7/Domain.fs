﻿module aoc24.day7.Domain

open System
open Vass.Stuff
open Vass.Stuff.Types

type Equation = {
    result: int64
    values: int64 list
}

type Operation = Add | Multiply

[<TailCall>]
let rec private countSolutionsRec result (operations: ('a -> 'a -> 'a) list) acc values =
    match values with
    | first :: rest ->
        let acc' = List.allPairs operations acc |> List.map (fun (op, v) -> op v first)

        countSolutionsRec result operations acc' rest
    | _ ->
        acc
        |> List.filter (fun result' -> result = result')
        |> List.length

let countSolutions operations (equation: Equation) =
    match equation.values with
    | first :: rest -> countSolutionsRec equation.result operations [first] rest
    | [] -> 0

let concatValues a b =
    let scale = 1.0 + Math.Floor (Math.Log10 (double b))
    let a' = a * int64 (Math.Pow(10, scale))
    a' + b

let part1 input =
    input
    |> List.filter (fun eqn -> (countSolutions [(+); (*)] eqn) > 0)
    |> List.sumBy _.result
    |> Some

let part2 input =
    input
    |> List.filter (fun eqn -> (countSolutions [(+); (*); concatValues] eqn) > 0)
    |> List.sumBy _.result
    |> Some
