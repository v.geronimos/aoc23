﻿module day8.Domain

type NodeName = string
type Node = NodeName * NodeName
type NodeMap = Map<NodeName, Node>

type Direction =
    | Left
    | Right

let nextNode direction (node: Node) =
    match direction with
    | Left -> fst node
    | Right -> snd node

let loopingSeq input =
    seq {
        while true do
            yield! input
    }

let followNodeMap nodeMap directions (start: NodeName) =
    let nextNodeMapEntry currentNodeName nextDirection =
        nodeMap
        |> Map.find currentNodeName
        |> nextNode nextDirection

    loopingSeq directions
    |> Seq.scan nextNodeMapEntry start
