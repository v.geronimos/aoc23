﻿open System.IO
open Vass.Stuff
open day8.Domain

let transpose (xss: 'a seq list): 'a list seq =
    seq {
        let iterators = xss |> List.map (fun xs -> xs.GetEnumerator())
        while (iterators |> List.forall (fun xi -> xi.MoveNext())) do
            yield iterators |> List.map (fun xi -> xi.Current)
    }

let solve file =
    printfn $"Solving file %s{file}..."

    let directions, nodeMap =
        match (File.ReadAllText file |> Parser.parse day8.Parser.inputParser) with
        | Some stuff -> stuff
        | None -> raise (exn ($"could not read %A{file}"))

    if (nodeMap |> (Map.containsKey "AAA")) then
        let path =
            followNodeMap nodeMap directions "AAA"
            |> Seq.takeWhile ((<>) (NodeName "ZZZ"))

        let part1 = Seq.length path

        printfn $"Part 1 output: %A{part1}"

    let ghostStartNodes =
        nodeMap
        |> Map.keys
        |> Seq.filter (fun k -> k.EndsWith "A")
        |> List.ofSeq

    let paths =
        ghostStartNodes
        |> List.map (followNodeMap nodeMap directions)
        |> transpose
        |> Seq.takeWhile (fun currentNodes ->
            not (currentNodes |> Seq.forall (fun nodeName -> nodeName.EndsWith("Z"))))

    let part2 = Seq.length paths

    printfn $"Part 2 output: %A{part2}"

solve "example1.txt"
solve "example2.txt"
solve "example3.txt"
solve "input.txt"
