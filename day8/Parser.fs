﻿module day8.Parser

open System
open Vass.Stuff.Parser
open day8.Domain

let directions = literalMap (Map.ofList [ "L", Left; "R", Right ]) |> list

let nodeName (input: string, index) =
    if index + 3 >= input.Length then
        None
    else
        let name = input.Substring(index, 3)

        if (name |> Seq.forall Char.IsAsciiLetterOrDigit) then
            Some(NodeName name, index + 3)
        else
            None

let node =
    combine5 (literal "(") nodeName (literal ", ") nodeName (literal ")")
    |> map (fun (_, l, _, r, _) -> Node(l, r))

let nodeMapEntry =
    combine3 nodeName (literal " = ") node |> map (fun (name, _, n) -> name, n)

let nodeMap = separatedList newline nodeMapEntry |> map Map.ofList

let inputParser =
    combine3 directions (seq newline) nodeMap |> map (fun (d, _, nm) -> d, nm)
