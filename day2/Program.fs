﻿open System
open System.IO
open Microsoft.FSharp.Collections
open Vass.Stuff

open day2.Types

let parameters =
    match File.ReadAllLines "params.txt" |> Seq.head |> Parser.parse day2.Parser.draws with
    | Some p -> p
    | None -> raise( Exception("couldn't read parameters"))

printfn $"Parameters: %A{parameters}"

let input = File.ReadAllLines "input.txt"
printfn $"Input has %A{input.Length} lines"

let games =
    input
    |> Array.choose (Parser.parse day2.Parser.game)
printfn $"Parsed %A{games.Length} games"

let validGames, invalidGames =
    games
    |> Array.partition (fun g ->
        g.draws
        |> Seq.forall (Map.forall (fun colour drawCount -> drawCount <= (Map.find colour parameters))))
printfn $"Part 1: %A{validGames.Length} valid, %A{invalidGames.Length} invalid"

let sum =
    validGames
    |> Array.sumBy (fun g -> g.id)

printfn $"Part 1 result: %A{sum}"

let minimums =
    games
    |> Array.map (fun game ->
        game.draws
        |> Seq.fold (fun acc draw ->
            acc
            |> Map.map (fun c n ->
                match Map.tryFind c draw with
                | Some n2 -> Int32.Max(n, n2)
                | None -> n)) (Map.ofList [Red, 0; Green, 0; Blue, 0]))
let power draw =
    (dcolour Red draw) * (dcolour Green draw) * (dcolour Blue draw)

let sum2 =
    minimums
    |> Array.map power
    |> Array.sum
printfn $"Part 2 output: %A{sum2}"
