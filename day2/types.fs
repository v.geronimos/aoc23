﻿module day2.Types

type Colour = Red | Green | Blue
type Draw = Map<Colour, int>
type Game = {
    id: int
    draws: Draw seq
}

let dcolour colour draw =
    let value = draw |> Map.tryFind colour
    match value with
    | Some n -> n
    | _ -> 0
