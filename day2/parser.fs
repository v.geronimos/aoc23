﻿module day2.Parser
    open Vass.Stuff
    open day2.Types

    let colour = Parser.literalMap (Map.ofList [
        "red", Red
        "green", Green
        "blue", Blue
    ])

    let draw = Parser.combine3 Parser.number32 Parser.whitespace colour |> Parser.map (fun (n, _, c) -> c, n)
    let draws = Parser.separatedSeq (Parser.literal ",") draw |> Parser.map Map.ofSeq
    let gameNumber =
        Parser.combine3 (Parser.literal "Game ") Parser.number32 (Parser.literal ": ")
        |> Parser.map (fun (_, n, _) -> n)

    let game =
        Parser.combine gameNumber (Parser.separatedSeq (Parser.literal ";") draws)
        |> Parser.map (fun (n, ds) -> { id = n; draws = ds })
