﻿module day7.Domain

type Card =
    | Joker = 1
    | Two = 2
    | Three = 3
    | Four = 4
    | Five = 5
    | Six = 6
    | Seven = 7
    | Eight = 8
    | Nine = 9
    | Ten = 10
    | Jack = 11
    | Queen = 12
    | King = 13
    | Ace = 14

type Hand = { cards: Card list; bid: int64 }

type HandType =
    | Crap
    | OnePair
    | TwoPair
    | ThreeOfAKind
    | FullHouse
    | FourOfAKind
    | FiveOfAKind

let handType hand =
    let counts =
        hand.cards
        |> List.fold
            (fun acc card ->
                let count = (Map.tryFind card acc) |> Option.defaultValue 0
                acc |> Map.add card (1 + count))
            Map.empty

    counts
    |> Map.toList
    |> List.sortByDescending fst
    |> List.fold
        (fun state (card, count) ->
            match card, count, state with
            | Card.Joker, 1, FourOfAKind -> FiveOfAKind
            | Card.Joker, 2, ThreeOfAKind -> FiveOfAKind
            | Card.Joker, 1, ThreeOfAKind -> FourOfAKind
            | Card.Joker, 1, TwoPair -> FullHouse
            | Card.Joker, 3, OnePair -> FiveOfAKind
            | Card.Joker, 2, OnePair -> FourOfAKind
            | Card.Joker, 1, OnePair -> ThreeOfAKind
            | Card.Joker, 4, Crap -> FiveOfAKind
            | Card.Joker, 3, Crap -> FourOfAKind
            | Card.Joker, 2, Crap -> ThreeOfAKind
            | Card.Joker, 1, Crap -> OnePair

            | _, 5, _ -> FiveOfAKind
            | _, _, FiveOfAKind -> FiveOfAKind

            | _, 4, _ -> FourOfAKind
            | _, _, FourOfAKind -> FourOfAKind

            | _, 3, OnePair -> FullHouse
            | _, 2, ThreeOfAKind -> FullHouse
            | _, _, FullHouse -> FullHouse

            | _, 3, _ -> ThreeOfAKind

            | _, 2, OnePair -> TwoPair

            | _, 2, Crap -> OnePair

            | _, _, x -> x)
        Crap

let compareHandType a b =
    let rank =
        function
        | Crap -> 1
        | OnePair -> 2
        | TwoPair -> 3
        | ThreeOfAKind -> 4
        | FullHouse -> 5
        | FourOfAKind -> 6
        | FiveOfAKind -> 7

    (rank b) - (rank a)

let bestOf a b =
    let comparison = compareHandType a b
    if comparison < 0 then a else b

let compareHand a b =
    let comparison = compareHandType (handType a) (handType b)

    if comparison <> 0 then
        comparison
    else
        (List.zip a.cards b.cards)
        |> List.tryPick (fun (cardA, cardB) ->
            if cardA = cardB then
                None
            else
                Some(int (cardB) - int (cardA)))
        |> Option.defaultValue 0

let convertJacksToJokers hand =
    { hand with
        cards =
            hand.cards
            |> List.map (function
                | Card.Jack -> Card.Joker
                | x -> x) }
