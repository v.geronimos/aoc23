﻿open System.IO
open Vass.Stuff
open day7.Domain

let solve file =
    printfn $"Solving file %s{file}..."

    let hands =
        match (File.ReadAllText file |> Parser.parse day7.Parser.inputParser) with
        | Some stuff -> stuff
        | None -> raise (exn ($"could not read %A{file}"))

    let part1 =
        hands
        |> List.sortWith compareHand
        |> List.rev
        |> List.mapi (fun i hand -> int64 (i + 1) * hand.bid)
        |> List.sum


    printfn $"Part 1 output: %A{part1}"

    let part2 =
        hands
        |> List.map convertJacksToJokers
        |> List.sortWith compareHand
        |> List.rev
        |> List.mapi (fun i hand -> int64 (i + 1) * hand.bid)
        |> List.sum

    printfn $"Part 2 output: %A{part2}"

solve "example.txt"
solve "input.txt"
