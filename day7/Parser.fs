﻿module day7.Parser

open Vass.Stuff.Parser
open day7.Domain

let (|CardNumber|_|) c =
    if '2' <= c && c <= '9'
    then Some (enum<Card>(int(c - '0')))
    else None

let card =
    character
    |> choose (
        function
        | 'A' -> Some Card.Ace
        | 'K' -> Some Card.King
        | 'Q' -> Some Card.Queen
        | 'J' -> Some Card.Jack
        | 'T' -> Some Card.Ten
        | CardNumber c -> Some c
        | _ -> None)

let hand =
    combine3 (repeat 5 card) whitespace number
    |> map (fun (cards, _, bid) -> { cards = cards; bid = bid })

let inputParser =
    separatedList newline hand
