﻿open System.IO
open Vass.Stuff
open day10.Domain

let solve file =
    printfn $"Solving file %s{file}..."

    let tileMap =
        match (File.ReadAllText file |> Parser.parse day10.Parser.inputParser) with
        | Some stuff -> stuff
        | None -> raise (exn ($"could not read %A{file}"))

    let startPosition = startLocation tileMap
    let path = traversePipe tileMap startPosition

    let part1 =
        path
        |> List.length

    printfn $"Part 1 output: %A{part1}"

    let part2 = "not done yet"

    printfn $"Part 2 output: %A{part2}"

solve "example1.txt"
solve "example2.txt"
solve "example3.txt"
solve "example4.txt"
solve "input.txt"
