﻿module day10.Domain

type Direction =
    | North
    | East
    | South
    | West

type PipeDirection =
    | NorthSouth
    | EastWest
    | NorthEast
    | SouthEast
    | SouthWest
    | NorthWest

type Tile =
    | Pipe of PipeDirection
    | Ground
    | Start

type TileMap = Tile list list

type Position = int * int

let (|NorthExit|Nope|) dir =
    match dir with
    | NorthSouth | NorthEast | NorthWest -> NorthExit
    | _ -> Nope
let (|EastExit|Nope|) dir =
    match dir with
    | NorthEast | EastWest | SouthEast -> EastExit
    | _ -> Nope
let (|SouthExit|Nope|) dir =
    match dir with
    | NorthSouth | SouthEast | SouthWest -> SouthExit
    | _ -> Nope
let (|WestExit|Nope|) dir =
    match dir with
    | NorthWest | EastWest | SouthWest -> WestExit
    | _ -> Nope

let startLocation tileMap: Position =
    tileMap
    |> List.indexed
    |> List.pick (fun (rowNumber, row) ->
        row
        |> List.indexed
        |> List.tryPick (fun (colNumber, col) -> match col with | Start -> Some (rowNumber, colNumber) | _ -> None))

let getTile (tileMap: TileMap) (r,c): Tile =
    if r < 0 || r >= tileMap.Length || c < 0 || c >= tileMap[r].Length then
        Ground
    else
        tileMap[r][c]

let move (r, c) direction =
    match direction with
    | North -> (r - 1, c)
    | East -> (r, c + 1)
    | South -> (r + 1, c)
    | West -> (r, c - 1)

let startToPipeDirection tileMap startPosition: PipeDirection =
    let gt = getTile tileMap
    let north = gt (move startPosition North)
    let east = gt (move startPosition East)
    let south = gt (move startPosition South)
    let west = gt (move startPosition West)

    match north, east, south, west with
    | Pipe SouthExit, Pipe WestExit, _, _ -> NorthEast
    | _, Pipe WestExit, Pipe NorthExit, _ -> SouthEast
    | _, _, Pipe NorthExit, Pipe EastExit -> SouthWest
    | Pipe SouthExit, _, _, Pipe EastExit -> NorthWest
    | Pipe SouthExit, _, Pipe NorthExit, _ -> NorthSouth
    | _, Pipe WestExit, _, Pipe EastExit -> EastWest
    | _, _, _, _ -> raise (exn $"couldn't figure out start pipe direction based on neighbours: North=%A{north} East=%A{east} South=%A{south} West=%A{west}")

let invert dir =
    match dir with
    | North -> South
    | East -> West
    | South -> North
    | West -> East

let otherDirection dir tile =
    match dir, tile with
    | East, Pipe NorthExit | South, Pipe NorthExit | West, Pipe NorthExit -> North
    | North, Pipe EastExit | South, Pipe EastExit | West, Pipe EastExit -> East
    | North, Pipe SouthExit | East, Pipe SouthExit | West, Pipe SouthExit -> South
    | North, Pipe WestExit | South, Pipe WestExit | East, Pipe WestExit -> West
    | _, _ -> raise (exn $"couldn't figure out non-%A{dir} exit from %A{tile}")

let traversePipe tileMap (startPosition: Position) =
    let startDirection = startToPipeDirection tileMap startPosition

    let startState =
        match startDirection with
        | NorthSouth -> (startPosition, North, startPosition, South)
        | EastWest -> (startPosition, East, startPosition, West)
        | NorthEast -> (startPosition, North, startPosition, East)
        | NorthWest -> (startPosition, North, startPosition, West)
        | SouthWest -> (startPosition, South, startPosition, West)
        | SouthEast -> (startPosition, South, startPosition, East)

    startState
    |> List.unfold (fun (lpos, ldir, rpos, rdir) ->
        if lpos = rpos && lpos <> startPosition then
            None
        else
            let lpos' = move lpos ldir
            let ltile' = getTile tileMap lpos'
            let ldir' = otherDirection (invert ldir) ltile'

            let rpos' = move rpos rdir
            let rtile' = getTile tileMap rpos'
            let rdir' = otherDirection (invert rdir) rtile'

            let state = (lpos', ldir', rpos', rdir')
            Some (state, state))
