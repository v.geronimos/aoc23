﻿module day10.Parser
open day10.Domain
open Vass.Stuff
open Vass.Stuff.Parser

let tile = literalMap (Map.ofList [
    "|", Pipe NorthSouth
    "-", Pipe EastWest
    "L", Pipe NorthEast
    "J", Pipe NorthWest
    "7", Pipe SouthWest
    "F", Pipe SouthEast
    ".", Ground
    "S", Start
])

let tileRow = list tile
let inputParser: TileMap ParseFunc = separatedList newline tileRow
