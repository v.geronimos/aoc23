﻿module day13.Domain

open Vass.Stuff.Types
open Vass.Stuff

type Tile =
    | Ash
    | Rocks

type Reflection =
    | Vertical of int
    | Horizontal of int

let splitVertical (tileGrid: Tile grid) columns =
    tileGrid[*, 0..(columns-1)], tileGrid[*, columns..]

let splitHorizontal (tileGrid: Tile grid) rows =
    tileGrid[0..(rows-1), *], tileGrid[rows.., *]

let findReflection (tileGrid: Tile grid) =
    let tryFindReflection (lenner: Tile grid -> int) (splitter: Tile grid -> int -> Tile grid * Tile grid) (revver: Tile grid -> Tile grid) =
        [1..(lenner tileGrid) - 2]
        |> List.tryPick (fun splitColumn ->
            let left, right = splitter tileGrid splitColumn
            let right' = revver right
            let width = min (lenner left) (lenner right)

        )
