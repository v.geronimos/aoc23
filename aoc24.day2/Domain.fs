﻿module aoc24.day2.Domain

type Report = Report of int64 list

type Direction =
    | Increasing
    | Decreasing
type SafetyWorking =
    | Safe of Direction
    | Unsafe

let categoriseTransition (x: int64, y: int64) =
    let diff = y - x
    if (diff < -3 || diff = 0 || diff > 3)
    then Unsafe
    else if (diff < 0)
    then Safe Decreasing
    else Safe Increasing

let isSafe (Report levels) =
    levels
    |> List.pairwise
    |> List.map categoriseTransition
    |> List.reduce (fun x y ->
        match (x, y) with
        | Safe x, Safe y when x = y -> Safe x
        | _, _ -> Unsafe)
    |> function
        | Safe _ -> true
        | Unsafe -> false

let isSafeEnough (Report levels) =
    levels
    |> Seq.mapi (fun i _ ->
        let modifiedList = levels |> List.removeAt i
        isSafe (Report modifiedList))
    |> Seq.exists id
