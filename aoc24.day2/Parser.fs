﻿module aoc24.day2.Parser

open Vass.Stuff.Parser
open aoc24.day2.Domain

let report =
    separatedList whitespace number
    |> choose (fun levels ->
        if (List.length levels > 0) then Some (Report levels) else None)

let inputParser =
    separatedList newline report
