﻿open Vass.Stuff
open aoc24.day2.Domain

let part1 reports =
    reports
    |> List.filter isSafe
    |> List.length
    |> Some

let part2 reports =
    reports
    |> List.filter isSafeEnough
    |> List.length
    |> Some

let solve = Solver.solve aoc24.day2.Parser.inputParser part1 part2

solve "example.txt"
solve "input.txt"
