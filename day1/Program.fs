﻿open System
open System.IO
open System.Linq
open Microsoft.FSharp.Core

let (|Digit|_|) (s: string) =
    let mutable value = 0
    if Int32.TryParse(s.Substring(0, 1), &value) then Some(value)
    else if s.StartsWith "one" then Some(1)
    else if s.StartsWith "two" then Some(2)
    else if s.StartsWith "three" then Some(3)
    else if s.StartsWith "four" then Some(4)
    else if s.StartsWith "five" then Some(5)
    else if s.StartsWith "six" then Some(6)
    else if s.StartsWith "seven" then Some(7)
    else if s.StartsWith "eight" then Some(8)
    else if s.StartsWith "nine" then Some(9)
    else if s.StartsWith "zero" then Some(0)
    else None

let numbers (s: string) =
    [0 .. s.Length - 1]
    |> Seq.choose (fun x -> match s.Substring(x) with | Digit d -> Some d | _ -> None)

let input = File.ReadAllLines("input.txt")
let ns =
    input
    |> Array.map (fun s -> s, numbers s)
    |> Array.map (fun (s, ns) -> (s, ns.First(), Seq.last ns))
let debug =
    String.Join ("\n", (ns |> Array.map (fun (s, a, b) -> $"%s{s}: %A{a} %A{b}")))

printf $"ns: %s{debug}\n"

let sum =
    ns
    |> Array.map (fun (s, a,b) -> a * 10 + b)
    |> Array.sum

printf $"sum: %A{sum}"
