﻿module aoc24.day13.Domain

open System
open Microsoft.FSharp.Core
open Vass.Stuff
open Vass.Stuff.Types

type Machine = {
    A: int64 * int64
    B: int64 * int64
    Prize: int64 * int64
}

let maxAttempts prize offsets =
    let px, py = prize
    let x, y = offsets

    let maxX = px / x
    let maxY = py / y

    Math.Min(int64 maxX, maxY)

let add (x1, y1) (x2, y2) = x1 + x2, y1 + y2

let attempt machine b =
    let ax, ay = machine.A
    let bx, by = machine.B
    let px, py = machine.Prize

    let px' = px - bx * b
    let py' = py - by * b

    let a1, r1 = px' / ax, px' % ax
    let a2, r2 = py' / ay, py' % ay
    if a1 = a2 && r1 = 0L && r2 = 0L
    then Some a1
    else None

let minToWinNaive machine =
    let maxB = maxAttempts machine.Prize machine.B

    let aCost = 3L
    let bCost = 1L

    seq { for b in maxB .. -1L .. 0L -> b }
    |> Seq.tryPick (fun b ->
        match attempt machine b with
        | None -> None
        | Some a ->
            let cost = a * aCost + b * bCost
            Some cost
        )


let part1 input =
    input
    |> Seq.choose minToWinNaive
    |> Seq.sum
    |> Some

let part2 input =
    None
