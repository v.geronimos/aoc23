﻿module aoc24.day13.Parser

open Vass.Stuff
open Vass.Stuff.Parser
open Domain

let offsets =
    combine4 (literal "X+") number (literal ", Y+") number
    |> map (fun (_,x,_,y) -> x,y)

let button label =
    combine4 (literal "Button ") (literal label) (literal ": ") offsets
    |> map (fun (_,_,_,offsets) -> offsets)

let prize =
    combine4 (literal "Prize: X=") number (literal ", Y=") number
    |> map (fun (_,x,_,y) -> x,y)

let machine =
    combine5 (button "A") newline (button "B") newline prize
    |> map (fun (a,_,b,_,prize) -> { A = a; B = b; Prize = prize })

let inputParser =
    separatedList (list newline) machine
