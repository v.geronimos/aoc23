﻿module day6.Parser

open Vass.Stuff.Parser
open day6.Domain

let time =
    combine3 (literal "Time:") whitespace (separatedSeq whitespace number)
    |> map (fun (_, _, numbers) -> numbers)

let distance =
    combine3 (literal "Distance:") whitespace (separatedSeq whitespace number)
    |> map (fun (_, _, numbers) -> numbers)

let inputParser =
    combine3 time newline distance
    |> map (fun (t, _, d) -> (Seq.zip t d) |> Seq.map (fun (t, d) -> { time = t * 1L<ms>; distance = d * 1L<mm> }) |> List.ofSeq)
