﻿open System.IO
open Vass.Stuff
open day6.Domain

let races =
    match (
        File.ReadAllText "input.txt"
        |> Parser.parse day6.Parser.inputParser) with
    | Some stuff -> stuff
    | None -> raise (exn("could not read input"))

let part1 =
    races
    |> List.map fasterCombinations
    |> List.reduce (*)
printfn $"Part 1 output: %A{part1}"

let singleRace =
    races
    |> List.fold (fun (time, distance) race -> time + race.time.ToString(), distance + race.distance.ToString()) ("", "")
    |> fun (time, distance) -> { time = int64(time) * 1L<ms>; distance = int64(distance) * 1L<mm> }

let part2 = fasterCombinations singleRace
printfn $"Part 2 output: %A{part2}"
