﻿module day6.Domain

[<Measure>] type ms
[<Measure>] type mm

type Race = {
    time: int64<ms>
    distance: int64<mm>
}

let distance raceTime holdTime =
    let acceleration = 1L<mm/ms/ms>
    let velocity = acceleration * holdTime
    velocity * (raceTime - holdTime)

let fasterCombinations race =
    [ 0L .. race.time * 1L<1/ms> ]
    |> List.map (((*) 1L<ms>) >> distance race.time)
    |> List.filter ((<) race.distance)
    |> List.length
