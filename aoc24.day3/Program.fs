﻿open Vass.Stuff
open aoc24.day3
open aoc24.day3.Domain

let part1 (tokens: Token seq) =
    tokens
    |> Seq.choose (function
        | Instruction (Mul (a, b)) -> Some (a * b)
        | _ -> None)
    |> Seq.sum
    |> Some

let part2 (tokens: Token seq) =
    tokens
    |> Seq.scan (fun (enabled, _) item ->
        match item with
        | Instruction Do -> Enabled, None
        | Instruction Don't -> Disabled, None
        | Instruction instr -> enabled, Some instr
        | _ -> enabled, None) (Enabled, None)
    |> Seq.choose (function
        | Enabled, Some (Mul (a, b)) -> Some (a * b)
        | _ -> None)
    |> Seq.sum
    |> Some



let solve = Solver.solve Parser.inputParser part1 part2

solve "example.txt"
solve "example2.txt"
solve "input.txt"
