﻿module aoc24.day3.Domain

type Instruction =
    | Mul of int64 * int64
    | Do
    | Don't

type InstructionState = Enabled | Disabled

type Token =
    | Instruction of Instruction
    | Crap
