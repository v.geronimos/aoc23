﻿module aoc24.day3.Parser

open System
open Domain
open Vass.Stuff.Parser

let singleChar =
    character
    |> choose (function
        | '(' -> Some "("
        | ')' -> Some ")"
        | ',' -> Some ","
        | _ -> None)

let openBracket = singleChar |> filter (function | "(" -> true | _ -> false)
let closeBracket = singleChar |> filter (function | ")" -> true | _ -> false)
let comma = singleChar |> filter (function | "," -> true | _ -> false)

let twoParams = combine5 openBracket number comma number closeBracket |> map (fun (_,a,_,b,_) -> a, b)

let instruction = oneOf [
    combine (literal "mul") twoParams |> map (fun (_, p) -> Instruction (Mul p))
    combine3 (literal "do") openBracket closeBracket |> map (fun _ -> Instruction Do)
    combine3 (literal "don't") openBracket closeBracket |> map (fun _ -> Instruction Don't)
]
let crap = instruction |> not |> map (fun _ -> Crap)

let inputParser = seq (oneOf [instruction; crap])
